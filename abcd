{
  "accounts_ad_scan": {
    "post": {
      "tags": [
        "AccountsADScanAPI"
      ],
      "summary": "Start a new AD scan for accounts",
      "operationId": "startAccountsADScan",
      "requestBody": {
        "description": "Metadata of AD scan for accounts",
        "content": {
          "application/json": {
            "schema": {
              "$ref": "../components/ad_components.json#/schemas/ADScanParameters"
            }
          }
        },
        "required": true
      },
      "responses": {
        "201": {
          "description": "Successful operation",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "../components/components.json#/schemas/UniqueIdObject"
              }
            }
          }
        },
        "default": {
          "description": "Unexpected error",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "../components/components.json#/schemas/ErrorModel"
              }
            }
          }
        }
      }
    }
  },
  "accounts_ad_scan_by_id": {
    "get": {
      "tags": [
        "AccountsADScanAPI"
      ],
      "summary": "Get metadata of AD scan for accounts by id",
      "description": "Get metadata of AD scan for accounts given its internal unique id",
      "operationId": "getAccountsADScan",
      "parameters": [
        {
          "name": "job_id",
          "in": "path",
          "description": "Id of AD scan for accounts",
          "schema": {
            "$ref": "../components/components.json#/schemas/UniqueId"
          },
          "required": true
        }
      ],
      "responses": {
        "200": {
          "description": "Successful operation",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "../components/ad_components.json#/schemas/ADScanParameters"
              }
            }
          }
        },
        "default": {
          "description": "Unexpected error",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "../components/components.json#/schemas/ErrorModel"
              }
            }
          }
        }
      }
    },
    "delete": {
      "tags": [
        "AccountsADScanAPI"
      ],
      "summary": "Delete AD scan for accounts",
      "description": "Stop scan and delete its metadata + scan result",
      "operationId": "deleteAccountsADScan",
      "parameters": [
        {
          "name": "job_id",
          "in": "path",
          "description": "Id of AD scan for accounts",
          "schema": {
            "$ref": "../components/components.json#/schemas/UniqueId"
          },
          "required": true
        }
      ],
      "responses": {
        "200": {
          "description": "Successful operation"
        },
        "default": {
          "description": "Unexpected error",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "../components/components.json#/schemas/ErrorModel"
              }
            }
          }
        }
      }
    }
  },
  "accounts_ad_scan_status_by_id": {
    "get": {
      "tags": [
        "AccountsADScanAPI"
      ],
      "summary": "Get status of AD scan for accounts by id",
      "description": "Get status of AD scan for accounts given its internal unique id",
      "operationId": "getAccountsADScanStatus",
      "parameters": [
        {
          "name": "job_id",
          "in": "path",
          "description": "Id of AD scan for accounts",
          "schema": {
            "$ref": "../components/components.json#/schemas/UniqueId"
          },
          "required": true
        }
      ],
      "responses": {
        "200": {
          "description": "Successful operation",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "../components/components.json#/schemas/ScanStatus"
              }
            }
          }
        },
        "default": {
          "description": "Unexpected error",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "../components/components.json#/schemas/ErrorModel"
              }
            }
          }
        }
      }
    }
  },
  "accounts_ad_scan_result_by_id": {
    "get": {
      "tags": [
        "AccountsADScanAPI"
      ],
      "summary": "Get result of AD scan for accounts by id",
      "description": "Get result of AD scan for accounts given its internal unique id",
      "operationId": "getAccountsADScanResults",
      "parameters": [
        {
          "name": "job_id",
          "in": "path",
          "description": "Account scan id",
          "schema": {
            "$ref": "../components/components.json#/schemas/UniqueId"
          },
          "required": true
        }
      ],
      "responses": {
        "200": {
          "description": "Successful operation",
          "content": {
            "application/json": {
              "schema": {
                "description": "Can be gzipped",
                "$ref": "../components/ad_components.json#/schemas/AccountsADScanResult"
              }
            }
          }
        },
        "default": {
          "description": "Unexpected error",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "../components/components.json#/schemas/ErrorModel"
              }
            }
          }
        }
      }
    }
  }
}
