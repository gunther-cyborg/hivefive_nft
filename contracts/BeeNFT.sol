// SPDX-License-Identifier: MIT

pragma solidity 0.8.4;

//import "OpenZeppelin/openzeppelin-contracts@4.3.2/contracts/token/ERC1155/ERC1155.sol";
//import "OpenZeppelin/openzeppelin-contracts@4.3.2/contracts/token/ERC721/ERC721.sol";
import "OpenZeppelin/openzeppelin-contracts@4.3.2/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "OpenZeppelin/openzeppelin-contracts@4.3.2/contracts/access/Ownable.sol";

contract BeeNFT is ERC721Enumerable, Ownable {
    // Metadata URIs
    string private metadataURI;
    string private hiddenURI;

    // Lottery
    bool private isLottery = false;
    address[] private concurrent;
    address[] private winners;

    // WhiteList
    address[] internal whitelistedAddresses;
    bool internal onlyWhitelisted = false;

    // Token intern rules
    bool private isPaused = false;
    bool private isHidden = true;
    uint8 private maxMintAmount = 20;
    uint8 private nftPerAddress = 3;
    uint256 private cost = 0.2 ether;
    uint256 private maxSupply = 10000;

    // NFT Attributes
    uint256 private hive;
    mapping(string => int8) characteristics;
    mapping(int8 => string) bonuses;

    constructor(
        string memory _name,
        string memory _symbol,
        string memory _metadataURI,
        string memory _hiddenURI
    ) ERC721(_name, _symbol) {
        setMetadataURI(_metadataURI);
        setHiddenURI(_hiddenURI);
    }

    function _baseURI() internal view virtual override returns (string memory) {
        return metadataURI;
    }

    /* Public functions */
    function tokenURI(uint256 tokenId) public view virtual override returns (string memory) {
        require(_exists(tokenId), "ERC721Metadata: URI query for nonexistent token");
        if (isHidden) {
            return hiddenURI;
        }
        string memory baseURI = _baseURI();
        return bytes(baseURI).length > 0
        ? string(abi.encodePacked(baseURI, tokenId))
        : "";
    }

    function participateToLottery() public {
        require(isLottery, "Lottery is not running at the moment");
        for (uint256 i = 0; i < concurrent.length; i++) {
            require(concurrent[i] != msg.sender,
                    "You cannot participate twice");
        }
        concurrent.push(msg.sender);
    }

    function mint(uint256 amountOfNft) public payable {
        require(!isPaused, "Contract is on pause");
        uint256 supply = totalSupply();
        require(amountOfNft > 0);
        require(amountOfNft <= maxMintAmount);
        require(supply + amountOfNft <= maxSupply);

        if (msg.sender != owner()) {
            // If we're on whitelisted presale
            // TODO refactor this variable to isSomethingRelated to presale
            if (onlyWhitelisted) {
                require(isWhiteListed(msg.sender), "Sender address not allowed to pre-sale");
                uint256 ownerTokenCount = balanceOf(msg.sender);
                require(ownerTokenCount < nftPerAddress, "You have mint every nft you can");
            }
            if (isLottery && winners.length > 0) {
                // Permit to mint for winners
                for (uint256 i = 0; i > winners.length; i++){
                    require(msg.sender == winners[i],
                        "This address are not among winners of the lottery");
                }
                require(balanceOf(msg.sender) < nftPerAddress, "You have mint every nft you can");
            }
            // Check if the price is enough to pay his amountOfNft
            require(msg.value >= cost * amountOfNft, "Too low");
        }
        for (uint256 i = 1; i <= amountOfNft; i++) {
            // safeMint get the address to send the nft
            // AND
            // a brand new tokenId so we need to get the
            // total supply of our contract and add i to
            // have rights tokenIds
            // Introduces different fees here
            _safeMint(msg.sender, supply + i);
        }
    }

    function isWhiteListed(address _user) public view returns (bool) {
        for (uint256 i = 0; i < whitelistedAddresses.length; i++) {
            if (whitelistedAddresses[i] == _user) {
                return true;
            }
        }
        return false;
    }

    /* Check the number of NFT are hold by the owner
     * @param address _owner : Address of owner
     * @return uint256[] tokenIds : Ids of tokens hold by _owner
     */
    function walletOfOwner(address _owner) public view returns (uint256[] memory) {
        uint256 ownerTokenCount = balanceOf(_owner);
        uint256[] memory tokenIds = new uint256[](ownerTokenCount);
        for (uint256 i; i < ownerTokenCount; i++) {
            tokenIds[i] = tokenOfOwnerByIndex(_owner, i);
        }
        return tokenIds;
    }
    // Whitelist for presale
    function whitelistUsers(address[] calldata _users) public onlyOwner {
        delete whitelistedAddresses;
        whitelistedAddresses = _users;
    }

    /* Owner functions */
    function reveal() public onlyOwner {
        isHidden = false;
    }

    /* Getters & Setters */
    function getWhitelistState() public view returns (bool){
        return onlyWhitelisted;
    }
    function getLotteryState() public view returns (bool) {
        return isLottery;
    }
    // TODO Rename this dumb variable
    function getConcurrent() public view returns (address[] memory) {
        return concurrent;
    }
    function getWinners() public view returns (address[] memory) {
        return winners;
    }

    function togglePreSale(bool _state) public onlyOwner {
        onlyWhitelisted = _state;
    }
    function togglePause(bool _state) public onlyOwner {
        isPaused = _state;
    }
    function toggleLottery(bool _state) public onlyOwner {
        isLottery = _state;
    }

    function setWinners(address[] memory _winners) public onlyOwner {
        winners = _winners;
    }

    function setMetadataURI(string memory _uri) public onlyOwner {
        metadataURI = _uri;
    }

    function setHiddenURI(string memory _uri) public onlyOwner {
        hiddenURI = _uri;
    }

    function setNftPerAddressLimit(uint8 _limit) public onlyOwner {
        nftPerAddress = _limit;
    }

    function setCost(uint256 _newCost) public onlyOwner {
        cost = _newCost;
    }

    function setMaxMintAmount(uint8 _newMaxMintAmount) public onlyOwner {
        maxMintAmount = _newMaxMintAmount;
    }

    function withdraw() public payable onlyOwner {
        (bool os,) = payable(owner()).call{value : address(this).balance}("");
        require(os);
    }
}
