import random
from typing import List

from brownie import BeeNFT, accounts, config
from helpers.scripts import get_account


def deploy():
    deployer = get_account()
    bee_nft_contract = BeeNFT.deploy("BeeNFT", "BNFT",
                                     "https://real.url.com/{id}.json",
                                     "https://hidden.url.com/hidden.json",
                                     {"from": deployer})


def main():
    deploy()


