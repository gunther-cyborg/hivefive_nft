from random import randint
from typing import List, Optional
from brownie import accounts, network, config


def get_account() -> str:
    if network.show_active() == "development":
        return accounts[0]
    elif config.get("wallets").get("from_key"):
        return accounts.load(config.get("wallets").get("from_key"))
    else:
        raise ValueError("You must set a private key to your own .env file"
                         f" in order to deploy to {network.show_active()}")


def chose_winners(addresses: List[str],
                  number_of_winners: int) -> Optional[List[str]]:
    # Guards
    if number_of_winners > len(addresses):
        raise ValueError("You cannot pick more winners "
                         "than there are participants")

    winners: List[str] = []
    for _ in range(number_of_winners):
        random_index: int = randint(0, len(addresses))
        if addresses[random_index] not in winners:
            winners.append(addresses[random_index])
    return winners
