from unittest import TestCase
import pytest
from web3 import Web3
from brownie import BeeNFT, accounts
from scripts.helpers.scripts import get_account, chose_winners


class TestBeeNFT(TestCase):

    def setUp(self) -> None:
        self.deployer = get_account()
        self.contract = BeeNFT.deploy("BeeNFT", "BNFT",
                                      "https://real.url.com/{id}.json",
                                      "https://hidden.url.com/hidden.json",
                                      {"from": self.deployer})

    def test_basic_local_deployment(self):
        self.assertTrue(len(BeeNFT) == 1)
        self.assertTrue(self.contract.address)

    def test_address_should_participate_to_lottery(self):
        bunch_of_addresses = accounts[1:-1]
        self.contract.toggleLottery(True, {"from": self.deployer})

        [
            self.contract.participateToLottery({"from": address})
            for address in bunch_of_addresses
        ]

        participants = self.contract.getConcurrent()
        self.assertEqual(len(participants),
                         len(bunch_of_addresses))
        self.assertEqual(participants, bunch_of_addresses)

    def test_winners_can_mint(self):
        bunch_of_addresses = accounts[1:-1]
        self.contract.toggleLottery(True, {"from": self.deployer})
        # Add participants to the lottery
        [
            self.contract.participateToLottery({"from": address})
            for address in bunch_of_addresses
        ]

        concurrent = self.contract.getConcurrent()
        winners = chose_winners(concurrent, 2)
        self.contract.setWinners(winners, {"from": self.deployer})
        nft_allowed_to_mint = 1

        # Selection of winners
        for winner in winners:
            self.contract.mint(nft_allowed_to_mint, {
                "from": winner,
                "value": Web3.toWei(200000000 + 100, "gwei")})

        # Verification of nft owned by any winners
        for winner in winners:
            nft_mint = self.contract.walletOfOwner(winner)
            self.assertEqual(len(nft_mint), nft_allowed_to_mint)
            # Test WIP
            self.assertTrue(False)

    def tearDown(self) -> None:
        BeeNFT.remove(self.contract)
